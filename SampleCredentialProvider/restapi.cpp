#include <cpprest/http_client.h>

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

using namespace std;

int main(int argc, char* argv[])
{
    // Make the request and asynchronously process the response.
    // It must start with http:// or https://
    http_client client(L"http://server2.freeddns.org:5000/api/v1/ping");

    client.request(methods::GET).then([](http_response response) {
        if (response.status_code() == status_codes::OK) {
            auto body = response.extract_string().get();
            wcout << body << endl;
        }}).wait();
        cout << "Hello world!" << endl;


        // Login

        http_client client2(L"http://server2.freeddns.org:5000/api/v1/login?username=superAdmin&credential=1234856");

        client2.request(methods::POST).then([](http_response response) {
            auto json = response.extract_json().get();
            if (response.status_code() == status_codes::OK) {

                auto token = json.at(U("data")).as_string();
                wcout << token << endl;
            }
            else {
                auto errorMsg = json.at(U("error")).as_string();
                wcout << errorMsg << endl;
            }

            }).wait();

            system("PAUSE");
            return 0;
}
