﻿#include <Windows.h>
#include <WinHttp.h>
#include <stdio.h>
#include <iostream> //getchar
#include <fstream>
#include <codecvt>
#include <string>
#define CURL_STATICLIB
#include <curl/curl.h>
#include <json/json.h>

#include <opencv2/opencv.hpp>
#include "helpers.h"
#include <cpprest/http_client.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <iostream>


#pragma comment(lib, "winhttp.lib")
#pragma warning( disable : 26451 )

using namespace cv;
using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

using namespace std;

struct LoginInfo
{
    string result;
    HRESULT status;
};

struct SessionData
{
    string sessionID;
    string userID;
    HRESULT status;
};

struct CameraResult
{
    string filePath;
    string fileName;
    HRESULT status;
};

size_t write_data(void* ptr, size_t size, size_t nmemb, void* stream) {
    string data((const char*)ptr, (size_t)size * nmemb);
    *((std::stringstream*)stream) << data << endl;
    return size * nmemb;
}

wstring utf8ToUtf16(const std::string& utf8Str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
    return conv.from_bytes(utf8Str);
}

string utf16ToUtf8(const std::wstring& utf16Str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
    return conv.to_bytes(utf16Str);
}

wstring get_utf16(const std::string& str, int codepage)
{
    if (str.empty()) return std::wstring();
    int sz = MultiByteToWideChar(codepage, 0, &str[0], (int)str.size(), 0, 0);
    std::wstring res(sz, 0);
    MultiByteToWideChar(codepage, 0, &str[0], (int)str.size(), &res[0], sz);
    return res;
}

string getWithHeader(LPCWSTR server, int port, LPCWSTR path, LPCWSTR header)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
    LPSTR pszOutBuffer;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL,
        hConnect = NULL,
        hRequest = NULL;
    string response;
    // Use WinHttpOpen to obtain a session handle.
    hSession = WinHttpOpen(L"WinHTTP Example/1.0",
        WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
        WINHTTP_NO_PROXY_NAME,
        WINHTTP_NO_PROXY_BYPASS, 0);

    // Specify an HTTP server.
    if (hSession)
        hConnect = WinHttpConnect(hSession, server,
            port, 0);

    // Create an HTTP request handle.
    if (hConnect)
        hRequest = WinHttpOpenRequest(hConnect, L"GET", path,
            NULL, WINHTTP_NO_REFERER,
            WINHTTP_DEFAULT_ACCEPT_TYPES,
            WINHTTP_FLAG_SECURE);

    // Send a request.
    if (hRequest)
        bResults = WinHttpSendRequest(hRequest,
            header, -1,
            NULL, 0, 0, 0);


    // End the request.
    if (bResults)
        bResults = WinHttpReceiveResponse(hRequest, NULL);

    // Keep checking for data until there is nothing left.
    if (bResults)
    {
        do
        {
            // Check for available data.
            dwSize = 0;
            if (!WinHttpQueryDataAvailable(hRequest, &dwSize))
                printf("Error %u in WinHttpQueryDataAvailable.\n",
                    GetLastError());

            // Allocate space for the buffer.
            pszOutBuffer = new char[dwSize + 1];
            if (!pszOutBuffer)
            {
                printf("Out of memory\n");
                dwSize = 0;
            }
            else
            {
                // Read the data.
                ZeroMemory(pszOutBuffer, dwSize + 1);

                if (!WinHttpReadData(hRequest, (LPVOID)pszOutBuffer,
                    dwSize, &dwDownloaded))
                    printf("Error %u in WinHttpReadData.\n", GetLastError());
                else
                    response = response + string(pszOutBuffer);
                // Free the memory allocated to the buffer.
                delete[] pszOutBuffer;
            }
        } while (dwSize > 0);
    }


    // Report any errors.
    if (!bResults)
        printf("Error %d has occurred.\n", GetLastError());

    // Close any open handles.
    if (hRequest) WinHttpCloseHandle(hRequest);
    if (hConnect) WinHttpCloseHandle(hConnect);
    if (hSession) WinHttpCloseHandle(hSession);
    return response;
}

string postWithHeader(LPCWSTR server, int port, LPCWSTR path, LPCWSTR header, string form)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
    LPSTR pszOutBuffer;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL,
        hConnect = NULL,
        hRequest = NULL;

    LPSTR data = const_cast<char*>(form.c_str());
    DWORD data_len = strlen(data);

    string response;
    // Use WinHttpOpen to obtain a session handle.
    hSession = WinHttpOpen(L"WinHTTP Example/1.0",
        WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
        WINHTTP_NO_PROXY_NAME,
        WINHTTP_NO_PROXY_BYPASS, 0);

    // Specify an HTTP server.
    if (hSession)
        hConnect = WinHttpConnect(hSession, server,
            port, 0);

    // Create an HTTP request handle.
    if (hConnect)
        hRequest = WinHttpOpenRequest(hConnect, L"POST", path,
            NULL, WINHTTP_NO_REFERER,
            WINHTTP_DEFAULT_ACCEPT_TYPES,
            WINHTTP_FLAG_SECURE);

    // Send a request.
    if (hRequest)
        bResults = WinHttpSendRequest(hRequest,
            header, -1,
            (LPVOID)data, data_len, data_len, 0);

    // End the request.
    if (bResults)
        bResults = WinHttpReceiveResponse(hRequest, NULL);

    // Keep checking for data until there is nothing left.
    if (bResults)
    {
        do
        {
            // Check for available data.
            dwSize = 0;
            if (!WinHttpQueryDataAvailable(hRequest, &dwSize))
                printf("Error %u in WinHttpQueryDataAvailable.\n",
                    GetLastError());

            // Allocate space for the buffer.
            pszOutBuffer = new char[dwSize + 1];
            if (!pszOutBuffer)
            {
                printf("Out of memory\n");
                dwSize = 0;
            }
            else
            {
                // Read the data.
                ZeroMemory(pszOutBuffer, dwSize + 1);

                if (!WinHttpReadData(hRequest, (LPVOID)pszOutBuffer,
                    dwSize, &dwDownloaded))
                    printf("Error %u in WinHttpReadData.\n", GetLastError());
                else
                    response = response + string(pszOutBuffer);
                // Free the memory allocated to the buffer.
                delete[] pszOutBuffer;
            }
        } while (dwSize > 0);
    }


    // Report any errors.
    if (!bResults)
        printf("Error %d has occurred.\n", GetLastError());

    // Close any open handles.
    if (hRequest) WinHttpCloseHandle(hRequest);
    if (hConnect) WinHttpCloseHandle(hConnect);
    if (hSession) WinHttpCloseHandle(hSession);
    return response;
}

string postWithoutHeader(LPCWSTR server, int port, LPCWSTR path, string form)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
    LPSTR pszOutBuffer;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL,
        hConnect = NULL,
        hRequest = NULL;

    LPSTR data = const_cast<char*>(form.c_str());
    DWORD data_len = strlen(data);

    string response;
    // Use WinHttpOpen to obtain a session handle.
    hSession = WinHttpOpen(L"WinHTTP Example/1.0",
        WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
        WINHTTP_NO_PROXY_NAME,
        WINHTTP_NO_PROXY_BYPASS, 0);

    // Specify an HTTP server.
    if (hSession)
        hConnect = WinHttpConnect(hSession, server,
            port, 0);

    // Create an HTTP request handle.
    if (hConnect)
        hRequest = WinHttpOpenRequest(hConnect, L"POST", path,
            NULL, WINHTTP_NO_REFERER,
            WINHTTP_DEFAULT_ACCEPT_TYPES,
            WINHTTP_FLAG_SECURE);

    // Send a request.
    if (hRequest)
        bResults = WinHttpSendRequest(hRequest,
            WINHTTP_NO_ADDITIONAL_HEADERS, 0,
            (LPVOID)data, data_len, data_len, 0);

    // End the request.
    if (bResults)
        bResults = WinHttpReceiveResponse(hRequest, NULL);

    // Keep checking for data until there is nothing left.
    if (bResults)
    {
        do
        {
            // Check for available data.
            dwSize = 0;
            if (!WinHttpQueryDataAvailable(hRequest, &dwSize))
                printf("Error %u in WinHttpQueryDataAvailable.\n",
                    GetLastError());

            // Allocate space for the buffer.
            pszOutBuffer = new char[dwSize + 1];
            if (!pszOutBuffer)
            {
                printf("Out of memory\n");
                dwSize = 0;
            }
            else
            {
                // Read the data.
                ZeroMemory(pszOutBuffer, dwSize + 1);

                if (!WinHttpReadData(hRequest, (LPVOID)pszOutBuffer,
                    dwSize, &dwDownloaded))
                    printf("Error %u in WinHttpReadData.\n", GetLastError());
                else
                    response = response + string(pszOutBuffer);
                // Free the memory allocated to the buffer.
                delete[] pszOutBuffer;
            }
        } while (dwSize > 0);
    }


    // Report any errors.
    if (!bResults)
        printf("Error %d has occurred.\n", GetLastError());

    // Close any open handles.
    if (hRequest) WinHttpCloseHandle(hRequest);
    if (hConnect) WinHttpCloseHandle(hConnect);
    if (hSession) WinHttpCloseHandle(hSession);
    return response;
}

std::wstring s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}

bool twoFA(string username, string otp)
{
    string header = "Content-Type: application/x-www-form-urlencoded\r\nAuthentication: ";
    std::wstring stemp = s2ws(header);
    LPCWSTR headerW = stemp.c_str();
    string token = postWithHeader(L"server2.freeddns.org", 5000, L"/api/v1/auth", headerW, "user_id=admin&credential=parallelcore");
    header = header.append(token);
    header = header.append("\r\n");
    stemp = s2ws(header);
    headerW = stemp.c_str();
    WriteLogFile(token.c_str());

    string url = "/api/v1/twofactor/secret-key/get?user_id=";
    url = url.append(username);
    WriteLogFile(url.c_str());

    wstring urlW = s2ws(url);
    string key = getWithHeader(L"server2.freeddns.org", 5000, urlW.c_str(), headerW);
    string form = "tempSecret=";
    form = form.append(key);
    form = form.append("&totpToken=");
    form = form.append(otp);
    WriteLogFile(form.c_str());

    string twoFA_passed = postWithHeader(L"server2.freeddns.org", 5000, L"/api/v1/twofactor/verify", headerW, form);
    if (twoFA_passed.compare("Authenticated") == 0)
    {
        WriteLogFile("pass");
        return true;
    }
    else 
    {
        WriteLogFile("fail");
        return false;
    }        
}

HRESULT otp_request(
    _In_ PCWSTR username,
    _In_ PCWSTR OTP
)
{
    string username2 = utf16ToUtf8(username);
    string OTP2 = utf16ToUtf8(OTP);    
    if (twoFA(username2, OTP2))
        return OTP_SUCCESS;
    return OTP_UNKNOWN_ERROR;
}

// Login bbchain with username and password
// Return Login info with token and status
LoginInfo login(string bbchain_username, string bbchain_password, string apiEndpoint)
{
    struct LoginInfo loginInfo;

    // Use libcurl to do request
    CURL* curl;
    CURLcode ret;
    curl = curl_easy_init();
    struct curl_httppost* post = NULL;
    struct curl_httppost* last = NULL;
    std::stringstream out;
    string apiURL = apiEndpoint + "login";
    WriteLogFile(apiURL.c_str());

    if (curl)
    {
        // Form data
        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "username",
            CURLFORM_COPYCONTENTS, bbchain_username,
            CURLFORM_END);

        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "credential",
            CURLFORM_COPYCONTENTS, bbchain_password,
            CURLFORM_END);
        
        curl_easy_setopt(curl, CURLOPT_URL, apiURL.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);

        curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

        ret = curl_easy_perform(curl);
        if (ret)
        {
            // If there is error in curl
            WriteLogFile(curl_easy_strerror(ret));
            WriteLogFile("Error in curl");
            loginInfo.result = out.str();
            loginInfo.status = E_FAIL;
            return loginInfo;
        }

        curl_formfree(post);
    }
    else
    {
        // Error in curl
        loginInfo.result = out.str();
        loginInfo.status = E_FAIL;
    }

    // Parse Response
    string str_json = out.str();
    Json::Value obj;
    JSONCPP_STRING err;
    const auto rawJsonLength = static_cast<int>(str_json.length());
    Json::CharReaderBuilder builder;
    const unique_ptr<Json::CharReader> reader(builder.newCharReader());
    if (!reader->parse(str_json.c_str(), str_json.c_str() + rawJsonLength, &obj, &err))
    {
        loginInfo.result = "Json parsing failed";
        loginInfo.status = E_FAIL;
    }

    string token = obj["data"].asString();
    string error = obj["error"].asString();
    if (error.length() == 0 && token.length() != 0)
    {
        loginInfo.result = token;
        loginInfo.status = S_OK;
    }
    else {
        loginInfo.result = error;
        loginInfo.status = E_FAIL;
    }
   
    curl_easy_cleanup(curl);
    return loginInfo;
    
    // Using cpprestsdk to do request
    /*string apiRequest = "http://server2.freeddns.org:5000/api/v1/login?username=" + bbchain_username + "&credential=" + bbchain_password;
    WriteLogFile(apiRequest.c_str());
    wstring wideApiRequest;

    for (int i = 0; i < apiRequest.length(); i++)
    {
        wideApiRequest += wchar_t(apiRequest[i]);
    }
    http_client client(wideApiRequest);
    WriteLogFile(wideApiRequest.c_str());

    http_response response = client.request(methods::POST).get();
    auto json = response.extract_json().get();
    if (response.status_code() == status_codes::OK)
    {
        WriteLogFile("Success");
        loginInfo.status = S_OK;
        loginInfo.result = json.at(U("data")).as_string();
    } else
    {
        WriteLogFile("Failed");
        loginInfo.status = E_FAIL;
        loginInfo.result = json.at(U("error")).as_string();
    }
    return loginInfo;*/


    //async
    /*client.request(methods::POST).then([](http_response response) {
        auto json = response.extract_json().get();
        if (response.status_code() == status_codes::OK) {

            auto token = json.at(U("data")).as_string();
            WriteLogFile("Success");
            WriteLogFile(token.c_str());
            return token;
        }
        else {
            auto errorMsg = json.at(U("error")).as_string();
            WriteLogFile("Failed");
            WriteLogFile(errorMsg.c_str());
            return errorMsg;
        }

        }).wait();*/

    //WriteLogFile(bbchain_username.c_str());
    //WriteLogFile(bbchain_password.c_str());

    //string formStr = "username=" + bbchain_username + "&credential=" + bbchain_password;
    //WriteLogFile(formStr.c_str());
    //string token = postWithoutHeader(L"http://server2.freeddns.org", 5000, L"/api/v1/login", formStr);
    //header = header.append(token);
    //header = header.append("\r\n");
    //WriteLogFile(token.c_str());

    /*string url = "/api/v1/twofactor/secret-key/get?user_id=";
    url = url.append(username);
    WriteLogFile(url.c_str());

    wstring urlW = s2ws(url);
    string key = getWithHeader(L"server2.freeddns.org", 5000, urlW.c_str(), headerW);
    string form = "tempSecret=";
    form = form.append(key);
    form = form.append("&totpToken=");
    form = form.append(otp);
    WriteLogFile(form.c_str());

    string twoFA_passed = postWithHeader(L"server2.freeddns.org", 5000, L"/api/v1/twofactor/verify", headerW, form);
    if (twoFA_passed.compare("Authenticated") == 0)
    {
        WriteLogFile("pass");
        return true;
    }
    else
    {
        WriteLogFile("fail");
        return false;
    }*/
}

LoginInfo login_request(
    _In_ PCWSTR bbchain_username,
    _In_ PCWSTR bbchain_password,
    string apiEndpoint
    )
{
    string bbchain_username2 = utf16ToUtf8(bbchain_username);
    string bbchain_password2 = utf16ToUtf8(bbchain_password);
    LoginInfo outputResult = login(bbchain_username2, bbchain_password2, apiEndpoint);
    return outputResult;
}

// To post an image to server
// Get the score from the server
// If the score < 1 -> face login passed
// Otherwise, face login failed
HRESULT face_login(string token, string filePath, string fileName, string apiEndpoint)
{
    string authHeader = "Authorization: " + token;
    WriteLogFile(token.c_str());
    WriteLogFile(filePath.c_str());
    WriteLogFile(fileName.c_str());
    CURL* curl;
    CURLcode ret;
    curl = curl_easy_init();
    struct curl_httppost* post = NULL;
    struct curl_httppost* last = NULL;
    struct curl_slist* list = NULL;
    std::stringstream out;
    string apiURL = apiEndpoint + "facial/calculate";

    if (curl)
    {
        // Form Data
        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "image",
            CURLFORM_FILE, filePath.c_str(),
            CURLFORM_FILENAME, fileName.c_str(),
            CURLFORM_CONTENTTYPE, "image/jpeg",
            CURLFORM_END);

        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "sessionId",
            CURLFORM_COPYCONTENTS, "111",
            CURLFORM_END);

        curl_easy_setopt(curl, CURLOPT_URL, apiURL.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);

        list = curl_slist_append(list, authHeader.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

        ret = curl_easy_perform(curl);
        if (ret)
        {
            WriteLogFile(curl_easy_strerror(ret));
            WriteLogFile(out.str().c_str());
            WriteLogFile("API Face request failed");
            return E_FAIL;
        }

        curl_formfree(post);
        curl_slist_free_all(list);
    }
    else
    {
        return E_FAIL;
    }

    // Parse Response
    string str_json = out.str();
    WriteLogFile(str_json.c_str());

    Json::Value obj;
    JSONCPP_STRING err;
    const auto rawJsonLength = static_cast<int>(str_json.length());
    Json::CharReaderBuilder builder;
    const unique_ptr<Json::CharReader> reader(builder.newCharReader());
    if (!reader->parse(str_json.c_str(), str_json.c_str() + rawJsonLength, &obj, &err))
    {
        WriteLogFile("ERROR when parsing json");
        return E_FAIL;
    }

    string scoreStr = obj["data"].asString();
    string faceError = obj["error"].asString();
    WriteLogFile("SCORE: ");
    WriteLogFile(scoreStr.c_str());

    if (scoreStr.length() == 0)
    {
        WriteLogFile(faceError.c_str());
        return E_FAIL;
    }
    double score = atof(scoreStr.c_str());

    // If Score > 1 -> not the owner
    if (score > 1) {
        WriteLogFile("Score > 1");
        return E_FAIL;
    }
    curl_easy_cleanup(curl);
    return S_OK;
}

// User login with face and macID to start a session
// Return session data json string
SessionData startSession(string token, string macID, string filePath, string fileName, string apiEndpoint)
{
    struct SessionData sessionData;

    string authHeader = "Authorization: " + token;
    WriteLogFile(token.c_str());
    WriteLogFile(filePath.c_str());
    WriteLogFile(fileName.c_str());
    CURL* curl;
    CURLcode ret;
    curl = curl_easy_init();
    struct curl_httppost* post = NULL;
    struct curl_httppost* last = NULL;
    struct curl_slist* list = NULL;
    std::stringstream out;
    string apiURL = apiEndpoint + "session/start";

    if (curl)
    {
        // Form Data
        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "image",
            CURLFORM_FILE, filePath.c_str(),
            CURLFORM_FILENAME, fileName.c_str(),
            CURLFORM_CONTENTTYPE, "image/jpeg",
            CURLFORM_END);

        curl_formadd(&post, &last,
            CURLFORM_COPYNAME, "macId",
            CURLFORM_COPYCONTENTS, macID.c_str(),
            CURLFORM_END);

        curl_easy_setopt(curl, CURLOPT_URL, apiURL.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);

        list = curl_slist_append(list, authHeader.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

        ret = curl_easy_perform(curl);
        if (ret)
        {
            WriteLogFile(curl_easy_strerror(ret));
            WriteLogFile(out.str().c_str());
            WriteLogFile("Session Start Failed");
            sessionData.sessionID = "";
            sessionData.userID = "";
            sessionData.status = E_FAIL;
            return sessionData;
        }

        curl_formfree(post);
        curl_slist_free_all(list);
    }
    else
    {
        sessionData.sessionID = "";
        sessionData.userID = "";
        sessionData.status = E_FAIL;
        return sessionData;
    }

    // Parse Response
    string str_json = out.str();
    WriteLogFile(str_json.c_str());

    Json::Value obj;
    Json::Value dataObj;
    Json::FastWriter writer;
    JSONCPP_STRING err;
    const auto rawJsonLength = static_cast<int>(str_json.length());
    Json::CharReaderBuilder builder;
    const unique_ptr<Json::CharReader> reader(builder.newCharReader());
    if (!reader->parse(str_json.c_str(), str_json.c_str() + rawJsonLength, &obj, &err))
    {
        WriteLogFile("ERROR when parsing json");
        sessionData.sessionID = "";
        sessionData.userID = "";
        sessionData.status = E_FAIL;
        return sessionData;
    }

    //string sessionData = writer.write(obj["data"]);
    string sessionID = obj["data"]["ID"].asString();
    string userID = obj["data"]["UserID"].asString();
    string sessionError = obj["error"].asString();
    WriteLogFile(sessionID.c_str());
    WriteLogFile(userID.c_str());

    curl_easy_cleanup(curl);

    if (sessionError.length() != 0)
    {
        sessionData.sessionID = "";
        sessionData.userID = "";
        sessionData.status = E_FAIL;
        return sessionData;
    }
    sessionData.sessionID = sessionID;
    sessionData.userID = userID;
    sessionData.status = S_OK;
    return sessionData;
}

// Use opencv to access camera
// Capture one photo and write it on disk
// Return the cameraresult with stauts, filepath and filename
CameraResult OpenCamera()
{
    struct CameraResult cameraResult;
    VideoCapture cap(0); // open the default camera
    if (!cap.isOpened())  // check if we succeeded
    {
        WriteLogFile("OpenCamera() Failed");
        cameraResult.status = E_FAIL;
        cameraResult.filePath = "";
        cameraResult.fileName = "";
        return cameraResult;
    }
    Mat frame;
    cap >> frame; // get a new frame from camera
    // do any processing

    time_t rawtime;
    struct tm* timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%d-%m-%Y %H-%M-%S", timeinfo);
    string filePath = "C:/SafeGuardChain/captures/" + string(buffer) + ".jpg";
    imwrite(filePath, frame);
    cameraResult.status = S_OK;
    cameraResult.filePath = filePath;
    cameraResult.fileName = string(buffer) + ".jpg";
    return cameraResult;
}
