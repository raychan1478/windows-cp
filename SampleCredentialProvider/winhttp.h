#include <Windows.h>
#include <WinHttp.h>
#include <stdio.h>
#include <iostream> //getchar
#include <fstream>

#pragma comment(lib, "winhttp.lib")
#pragma warning( disable : 26451 )

using namespace std;

typedef struct LoginInfo
{
	string result;
	HRESULT status;
};

struct SessionData
{
	string sessionID;
	string userID;
	HRESULT status;
};

typedef struct CameraResult
{
	string filePath;
	string fileName;
	HRESULT status;
};


std::wstring get_utf16(const std::string& str, int codepage);
string HttpsWebRequestPost(string domain, string url, string dat);
string getWithHeader(LPCWSTR server, int port, LPCWSTR path, LPCWSTR header);
string postWithHeader(LPCWSTR server, int port, LPCWSTR path, LPCWSTR header, string form);
string utf16ToUtf8(const std::wstring& utf16Str);
std::wstring s2ws(const std::string& s);
bool twoFA(LPCWSTR server, int port, LPCWSTR path, string username, string otp);
HRESULT otp_request(
	_In_ PCWSTR username,
	_In_ PCWSTR OTP
);
LoginInfo login(string bbchain_username, string bbchain_password, string apiEndpoint);
LoginInfo login_request(
	_In_ PCWSTR bbchain_username,
	_In_ PCWSTR bbchain_password,
	string apiEndpoint
);
HRESULT face_login(string token, string filePath, string fileName, string apiEndpoint);
SessionData startSession(string token, string macID, string filePath, string fileName, string apiEndpoint);
CameraResult OpenCamera();